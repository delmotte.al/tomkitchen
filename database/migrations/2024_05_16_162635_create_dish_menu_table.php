<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dish_menu', function (Blueprint $table) {
            $table->id();
            $table->foreignId('soup_id')->references('id')->on('dishes');
            $table->foreignId('dish_id')->references('id')->on('dishes');
            $table->foreignId('dessert_id')->references('id')->on('dishes');
            $table->foreignId('menu_id')->references('id')->on('menus');
            $table->date('dish_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dish_menu');
    }
};
