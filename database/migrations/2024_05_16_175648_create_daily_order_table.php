<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('daily_order', function (Blueprint $table)
        {
            $table->id();
            $table->integer('soup_number')->nullable();
            $table->integer('dish_number')->nullable();
            $table->integer('dessert_number')->nullable();
            $table->foreignId('dish_menu_id')->references('id')->on('dish_menu');
            $table->foreignId('order_id')->references('id')->on('orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('daily_order');
    }
};
