<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('intolerance_special', function (Blueprint $table) {
            $table->id();
            $table->foreignId('special_id')->references('id')->on('specials');
            $table->foreignId('intolerance_id')->references('id')->on('intolerances');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('intolerance_special');
    }
};
