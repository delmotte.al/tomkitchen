<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dish_order_special', function (Blueprint $table)
        {
            $table->id();
            $table->foreignId('special_id')->references('id')->on('specials');
            $table->foreignId('daily_order_id')->references('id')->on('daily_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
         Schema::dropIfExists('dish_order_special');
    }
};
