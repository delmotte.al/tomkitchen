<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {



        for ($i = 1 ; $i < 31 ; $i++)
        {
            $randomSoup = DB::table('dishes')->select('id')->where('type',1)->inRandomOrder()->first();
            $randomDish = DB::table('dishes')->select('id')->where('type',2)->inRandomOrder()->first();
            $randomDessert = DB::table('dishes')->select('id')->where('type',3)->inRandomOrder()->first();


            DB::table('menus')->insert([
                'soup' => $randomSoup->id,
                'main_course' => $randomDish->id,
                'dessert' => $randomDessert->id,
                'date' => '2024-05-'.$i
            ]);
        }
    }
}

