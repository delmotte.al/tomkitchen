<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('deliveries')->insert([
            ['id' => 1, 'name' => 'verte', 'color' => '#008000'],
            ['id' => 2, 'name' => 'rose', 'color' => '#fd006b'],
            ['id' => 3, 'name' => 'bleue', 'color' => '#008080'],

        ]);
    }
}
