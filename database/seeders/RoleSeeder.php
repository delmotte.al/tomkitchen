<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('roles')->insert([
            ['id' => 1, 'name' => 'administrateur'],
            ['id' => 2, 'name' => 'chef'],
            ['id' => 3, 'name' => 'livreur'],
            ['id' => 4, 'name' => 'cuisinier'],
            ['id' => 5, 'name' => 'guest']
        ]);
    }
}
