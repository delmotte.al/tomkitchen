<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Special;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            DeliverySeeder::class,
            ClientSeeder::class,
            SpecialSeeder::class,
            RoleSeeder::class,
            UsersSeeder::class,
            TypeDishSeeder::class,
            DishSeeder::class,
            IntoleranceSeeder::class,
           // MenuSeeder::class
        ]);
    }
}
