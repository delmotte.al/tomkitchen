<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();


            DB::table('users')->insert([
                 [
                     'user_name' => "admin",
                     'first_name' => "Nathalie",
                     'name' => "Salmon",
                     'email' => "admin@gmail.com",
                     'phone' => "0471211601",
                     'password' => Hash::make('alex&&&&'),
                     'role_id' => 1,
                 ],
                [
                    'user_name' => "chef",
                    'first_name' => "Thomas",
                    'name' => "Lebrun",
                    'email' => "chef@gmail.com",
                    'phone' => "0471211600",
                    'password' => Hash::make('alex&&&&'),
                    'role_id' => 2,
                ],
                [
                    'user_name' => "livreur",
                    'first_name' => "bri",
                    'name' => "wineup",
                    'email' => "livreur@gmail.com",
                    'phone' => "0471211602",
                    'password' => Hash::make('alex&&&&'),
                    'role_id' => 3,
                ],
                [
                    'user_name' => "cuisinier",
                    'first_name' => "jo",
                    'name' => "le plusbo",
                    'email' => "cuisiner@gmail.com",
                    'phone' => "0471211604",
                    'password' => Hash::make('alex&&&&'),
                    'role_id' => 4,
                ],
                [
                    'user_name' => "guest",
                    'first_name' => "guest",
                    'name' => "guest",
                    'email' => "guest@gmail.com",
                    'phone' => "0471211605",
                    'password' => Hash::make('alex&&&&'),
                    'role_id' => 5,
                ],
                [
                    'user_name' => "alex",
                    'first_name' => "Alexandre",
                    'name' => "Delmotte",
                    'email' => "delmotte.al@gmail.com",
                    'phone' => "0471211603",
                    'password' => Hash::make('alex&&&&'),
                    'role_id' => 1,
                ],
            ]);
        }
}
