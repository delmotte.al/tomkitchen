<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpecialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0 ; $i < 5 ; $i++)
        {
            $specials = ['carbo' , 'bolo' , 'saussice stoemp' , 'boulette sauce tomate' , 'vol au vent'];

            DB::table('specials')->insert([
                'name' => $specials[$i],
                'is_active' => '1',
                'description' => '',
            ]);
        }
    }
}
