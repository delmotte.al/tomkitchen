<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {


        $status = ['1', '0'];

        for ($i = 1 ; $i < 16 ; $i++)
        {
            $soup = 'soup' . $i ;
            $dish = 'dish' . $i ;
            $dessert = 'dessert' . $i ;
            $randStatus = $status[array_rand($status)];


            DB::table('dishes')->insert([
                'name' => $soup,
                'type' => 1,
                'is_active' => $randStatus
            ]);
            DB::table('dishes')->insert([
                'name' => $dish,
                'type' => 2,
                'is_active' => $randStatus
            ]);
            DB::table('dishes')->insert([
                'name' => $dessert,
                'type' => 3,
                'is_active' => $randStatus
            ]);
        }
    }
}

