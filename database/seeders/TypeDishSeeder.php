<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeDishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('type_dish')->insert([
            ['id' => 1, 'name' => 'Soup'],
            ['id' => 2, 'name' => 'Main course'],
            ['id' => 3, 'name' => 'Dessert'],

        ]);
    }
}
