<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        foreach(range(1, 50) as $index)
        {
            $array = [1 , 2 , 3];
            $randKey = array_rand($array);
            $delivery = $array[$randKey];

            $username = $faker->unique()->name;
            $email = str_replace(' ', '', $username) . "@gmail.com";

            DB::table('clients')->insert([
                'name'    =>    $username,
                'address' =>  $faker->address ,
                'phone'   => $faker->phoneNumber(),
                'email'   =>    $email ,
                'delivery_id' => $delivery,
                ]);
        }
    }
}
