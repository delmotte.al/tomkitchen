<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntoleranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('intolerances')->insert([
            ['id' => 1, 'name' => 'sans sauce'],
            ['id' => 2, 'name' => 'coupé'],
            ['id' => 3, 'name' => 'végé'],
            ['id' => 4, 'name' => 'pas de sauce'],

        ]);
    }
}

