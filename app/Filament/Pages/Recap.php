<?php

namespace App\Filament\Pages;

use App\Models\DailyOrder;
use Filament\Pages\Page;


class Recap extends Page
{
    protected static ?string $navigationGroup = 'Recap';

    protected static string $view = 'filament.pages.recap';

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    public function get()
    {
        return DailyOrder::selectRaw('dish_menu_id, SUM(soup_number) AS sum_soup, SUM(dish_number) AS sum_dish, SUM(dessert_number) AS sum_dessert')
            ->groupBy('dish_menu_id')
            ->get();
    }
}

