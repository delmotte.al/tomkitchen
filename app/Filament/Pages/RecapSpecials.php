<?php

namespace App\Filament\Pages;


use App\Models\Special;
use Filament\Pages\Page;


class RecapSpecials extends Page
{
    protected static ?string $navigationGroup = 'Recap';

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $view = 'filament.pages.recap-specials';

    public function get()
    {
        return Special::selectRaw('specialS.name AS special, GROUP_CONCAT(clients.name, ", ") AS client')
            ->join('dish_order_special', 'specials.id', '=', 'dish_order_special.special_id')
            ->join('daily_order', 'dish_order_special.daily_order_id', '=', 'daily_order.id')
            ->join('dish_menu', 'daily_order.dish_menu_id', '=', 'dish_menu.id')
            ->join('orders', 'daily_order.order_id', '=', 'orders.id')
            ->join('clients', 'orders.client_id', '=', 'clients.id')
            ->whereDate('dish_menu.dish_at', '=', '2024-05-01')
            ->groupBy('special')
            ->orderBy('special')
            ->get();
    }
}
