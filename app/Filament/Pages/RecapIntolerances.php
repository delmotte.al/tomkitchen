<?php

namespace App\Filament\Pages;


use App\Models\Intolerance;
use Filament\Pages\Page;


class RecapIntolerances extends Page
{
    protected static ?string $navigationGroup = 'Recap';

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $view = 'filament.pages.recap-intolerances';

    public function get()
    {
        return Intolerance::selectRaw('intolerances.name AS intolerance, GROUP_CONCAT(clients.name, ", ") AS client')
            ->join('dish_intolerance', 'intolerances.id', '=', 'dish_intolerance.intolerance_id')
            ->join('dishes', 'dish_intolerance.dish_id', '=', 'dishes.id')
            ->join('dish_menu', function($join) {
                $join->on('dish_menu.soup_id', '=', 'dishes.id')
                    ->orOn('dish_menu.dish_id', '=', 'dishes.id')
                    ->orOn('dish_menu.dessert_id', '=', 'dishes.id');
            })
            ->join('daily_order', 'dish_menu.id', '=', 'daily_order.dish_menu_id')
            ->join('orders', 'daily_order.order_id', '=', 'orders.id')
            ->join('clients', 'orders.client_id', '=', 'clients.id')
            ->join('client_intolerance', function($join) {
                $join->on('client_intolerance.client_id', '=', 'clients.id')
                    ->on('client_intolerance.intolerance_id', '=', 'intolerances.id');
            })
            ->whereDate('dish_menu.dish_at', '=', '2024-05-01')
            ->where(function($query) {
                $query->where(function($query) {
                    $query->where('daily_order.soup_number', '>', 0)
                        ->whereColumn('dish_menu.soup_id', 'dishes.id');
                })
                    ->orWhere(function($query) {
                        $query->where('daily_order.dish_number', '>', 0)
                            ->whereColumn('dish_menu.dish_id', 'dishes.id');
                    })
                    ->orWhere(function($query) {
                        $query->where('daily_order.dessert_number', '>', 0)
                            ->whereColumn('dish_menu.dessert_id', 'dishes.id');
                    });
            })
            ->groupBy('intolerances.name')
            ->orderBy('intolerances.name')
            ->get();
    }
}

