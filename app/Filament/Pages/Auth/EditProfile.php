<?php

namespace App\Filament\Pages\Auth;

use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Pages\Auth\EditProfile as BaseEditProfile;

class EditProfile extends BaseEditProfile
{
    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('Personal information'))
                    ->schema([
                TextInput::make('user_name')
                    ->label(__('User name'))
                    ->required()
                    ->maxLength(255)
                    ->columnSpan(2),
                TextInput::make('first_name')
                    ->label(__('First name'))
                    ->required()
                    ->maxLength(255),
                     $this->getNameFormComponent(),
                    ])->columns(2),
                Section::make(__('Contact'))
                    ->schema([
                     $this->getEmailFormComponent(),
                     TextInput::make('phone')
                         ->label(__('Phone'))
                         ->required()
                         ->maxLength(12),
                    ])->columns(2),
                Section::make(__('Password'))
                    ->schema([
                        $this->getPasswordFormComponent(),
                        $this->getPasswordConfirmationFormComponent(),
                    ])->columns(2)
            ]);
    }
}
