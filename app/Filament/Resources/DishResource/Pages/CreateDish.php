<?php

namespace App\Filament\Resources\DishResource\Pages;

use App\Filament\Resources\DishResource;
use App\Filament\Resources\redirectToIndex;
use Filament\Resources\Pages\CreateRecord;

class CreateDish extends CreateRecord
{
    use RedirectToIndex;

    protected static string $resource = DishResource::class;
}
