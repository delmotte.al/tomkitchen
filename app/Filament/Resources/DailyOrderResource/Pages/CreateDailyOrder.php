<?php

namespace App\Filament\Resources\DailyOrderResource\Pages;

use App\Filament\Resources\DailyOrderResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateDailyOrder extends CreateRecord
{
    protected static string $resource = DailyOrderResource::class;
}
