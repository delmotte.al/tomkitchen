<?php

namespace App\Filament\Resources\DailyOrderResource\Pages;

use App\Filament\Resources\DailyOrderResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDailyOrder extends EditRecord
{
    protected static string $resource = DailyOrderResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
