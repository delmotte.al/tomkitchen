<?php

namespace App\Filament\Resources;

use App\Filament\Resources\DishMenuResource\Pages;
use App\Filament\Resources\DishMenuResource\RelationManagers;
use App\Models\DishMenu;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class DishMenuResource extends Resource
{
    protected static ?string $model = DishMenu::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationGroup = 'Dish';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('soup_id')
                    ->label(__('Soup'))
                    ->required(),
                TextInput::make('dish_id')
                    ->label(__('Main Course'))
                    ->required()
                    ->numeric(),
                TextInput::make('dessert_id')
                    ->label(__('Dessert'))
                    ->required()
                    ->numeric(),
                TextInput::make('menu_id')
                    ->label(__('Menu'))
                    ->required()
                    ->numeric(),
                Forms\Components\DatePicker::make('dish_at')
                    ->label(__('Day for the order'))
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('soupDish.name')
                    ->label(__('Soup'))
                    ->sortable(),
                Tables\Columns\TextColumn::make('mainCourseDish.name')
                    ->label(__('Main Course'))
                    ->sortable(),
                Tables\Columns\TextColumn::make('dessertDish.name')
                    ->label(__('Dessert'))
                    ->sortable(),
                Tables\Columns\TextColumn::make('menu.month')
                    ->label(__('Menu'))
                    ->sortable(),
                Tables\Columns\TextColumn::make('dish_at')
                    ->label(__('Menu of the day'))
                    ->date()
                    ->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDishMenus::route('/'),
            'create' => Pages\CreateDishMenu::route('/create'),
            'edit' => Pages\EditDishMenu::route('/{record}/edit'),
        ];
    }
    public static function getModelLabel(): string
    {
        return __('Menu of the day');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Menus of the day');
    }

}
