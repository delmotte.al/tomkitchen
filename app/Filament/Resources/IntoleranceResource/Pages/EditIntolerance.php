<?php

namespace App\Filament\Resources\IntoleranceResource\Pages;

use App\Filament\Resources\IntoleranceResource;
use App\Filament\Resources\RedirectToIndex;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditIntolerance extends EditRecord
{
    use RedirectToIndex;

    protected static string $resource = IntoleranceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
