<?php

namespace App\Filament\Resources\IntoleranceResource\Pages;

use App\Filament\Resources\IntoleranceResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListIntolerances extends ListRecords
{
    protected static string $resource = IntoleranceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
