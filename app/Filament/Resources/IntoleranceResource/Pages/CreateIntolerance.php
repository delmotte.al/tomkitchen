<?php

namespace App\Filament\Resources\IntoleranceResource\Pages;

use App\Filament\Resources\IntoleranceResource;
use App\Filament\Resources\RedirectToIndex;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateIntolerance extends CreateRecord
{
    use RedirectToIndex;

    protected static string $resource = IntoleranceResource::class;
}
