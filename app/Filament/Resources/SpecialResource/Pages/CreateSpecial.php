<?php

namespace App\Filament\Resources\SpecialResource\Pages;

use App\Filament\Resources\RedirectToIndex;
use App\Filament\Resources\SpecialResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSpecial extends CreateRecord
{
    use RedirectToIndex;

    protected static string $resource = SpecialResource::class;
}
