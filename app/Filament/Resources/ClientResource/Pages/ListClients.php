<?php

namespace App\Filament\Resources\ClientResource\Pages;

use App\Filament\Resources\ClientResource;
use App\Models\Delivery;
use Filament\Actions;
use Filament\Resources\Components\Tab;
use Filament\Resources\Pages\ListRecords;

class ListClients extends ListRecords
{
    protected static string $resource = ClientResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    public function getTabs():array
    {
        return [
            'all' => Tab::make('All'),
            'verte' => Tab::make('Verte')
            ->modifyQueryUsing(function ($query){
                return $query->whereHas('deliveries', function ($query) {
                    $query->where('name', 'verte');
                });
            }),
             'bleue' => Tab::make('Bleue')
        ->modifyQueryUsing(function ($query){
            return $query->whereHas('deliveries', function ($query) {
                $query->where('name', 'bleue');
            });
        }),
         'rose' => Tab::make('Rose')
        ->modifyQueryUsing(function ($query){
            return $query->whereHas('deliveries', function ($query) {
                $query->where('name', 'rose');
            });
        })

        ];
    }
}
