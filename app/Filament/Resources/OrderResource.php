<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OrderResource\Pages;
use App\Filament\Resources\OrderResource\RelationManagers;
use App\Models\Order;
use App\Models\Special;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class OrderResource extends Resource
{
    protected static ?string $model = Order::class;

    protected static ?string $navigationIcon = 'heroicon-o-shopping-cart';

    protected static ?string $navigationGroup = 'Client';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('Client information'))
                    ->schema([
                        Select::make('client_id')
                            ->label(__('Client'))
                            ->searchable()
                            ->relationship(name:'client', titleAttribute: 'name')
                            ->required(),
                        ]),
                        Section::make(__('Order information'))
                            ->schema([
                                Repeater::make('dailyOrders')
                                    ->label(__('Daily order'))
                                    ->relationship('dailyOrders')
                                    ->schema([
                                        TextInput::make('soup_number')
                                            ->label(__('Soup'))
                                            ->numeric(),
                                        TextInput::make('dish_number')
                                            ->label(__('Main course'))
                                            ->numeric(),
                                        TextInput::make('dessert_number')
                                            ->label(__('Dessert'))
                                            ->numeric(),
                                        Select::make('specials')
                                            ->label(__('Specials'))
                                            ->relationship(name:'specials', titleAttribute: 'name')
                                            ->multiple()
                                            ->searchable()
                                            ->preload(),
                                        Select::make('dish_menu_id')
                                            ->label(__('Menu of the day'))
                                            ->relationship(name:'dishMenu', titleAttribute: 'dish_at')
                                            ->searchable()
                                            ->preload(),
                                    ])->columns(3)
                            ])
                    ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make("client.name")
                    ->label(__('Client'))
                    ->sortable(),
                TextColumn::make("daily_orders_count")->counts('dailyOrders')
                    ->label(__('Number of orders')),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOrders::route('/'),
            'create' => Pages\CreateOrder::route('/create'),
            'edit' => Pages\EditOrder::route('/{record}/edit'),
        ];
    }
    public static function getModelLabel(): string
    {
        return __('Order');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Orders');
    }
}
