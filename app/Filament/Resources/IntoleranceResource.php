<?php

namespace App\Filament\Resources;

use App\Filament\Resources\IntoleranceResource\Pages;
use App\Filament\Resources\IntoleranceResource\RelationManagers;
use App\Models\Intolerance;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class IntoleranceResource extends Resource
{
    protected static ?string $model = Intolerance::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';
    protected static ?string $navigationGroup = 'Product';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label(__('name'))
                    ->required(),
                Forms\Components\TextInput::make('description')
                    ->label(__('description')),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label(__('name'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('description')
                    ->label(__('description'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListIntolerances::route('/'),
            'create' => Pages\CreateIntolerance::route('/create'),
            'edit' => Pages\EditIntolerance::route('/{record}/edit'),
        ];
    }
    public static function getModelLabel(): string
    {
        return __('Intolerance');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Intolerances');
    }
}
