<?php

namespace App\Filament\Resources;

use App\Filament\Resources\DailyOrderResource\Pages;
use App\Filament\Resources\DailyOrderResource\RelationManagers;
use App\Models\DailyOrder;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class DailyOrderResource extends Resource
{
    protected static ?string $model = DailyOrder::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationGroup = 'Client';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('soup_number')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('dish_number')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('dessert_number')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('dish_menu_id')
                    ->required()
                    ->numeric(),
                Forms\Components\Select::make('order_id')
                    ->relationship('order', 'id')
                    ->required(),
                Forms\Components\DatePicker::make('order_at')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('order.client.name')
                    ->badge()
                    ->color(fn (DailyOrder $client): string => match ($client->order->client->deliveries->name) {
                        'rose' => 'rose',
                        'verte' => 'verte',
                        'bleue' => 'bleue',
                    }),
                TextColumn::make('soup_number')
                    ->numeric()
                    ->sortable(),
                TextColumn::make('dish_number')
                    ->numeric()
                    ->sortable(),
                TextColumn::make('dessert_number')
                    ->numeric()
                    ->sortable(),
                TextColumn::make('dishMenu.dish_at')
                    ->sortable(),
                TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDailyOrders::route('/'),
            'create' => Pages\CreateDailyOrder::route('/create'),
            'edit' => Pages\EditDailyOrder::route('/{record}/edit'),
        ];
    }
    public static function getModelLabel(): string
    {
        return __('Daily order');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Daily orders');
    }
}
