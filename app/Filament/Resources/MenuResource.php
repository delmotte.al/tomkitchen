<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MenuResource\Pages;
use App\Filament\Resources\MenuResource\RelationManagers;
use App\Models\Dish;
use App\Models\Menu;
use Coolsam\FilamentFlatpickr\Forms\Components\Flatpickr;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class MenuResource extends Resource
{
    protected static ?string $model = Menu::class;

    protected static ?string $navigationIcon = 'heroicon-o-calendar-days';

    protected static ?string $navigationGroup = 'Dish';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Select::make('month')
                    ->options([
                        'Janvier' => 'Janvier',
                        'Février' => 'Février',
                        'Mars' =>'Mars',
                        'Avril' =>'Avril',
                        'Mai' =>'Mai',
                        'Juin' =>'Juin',
                        'Juillet' =>'Juillet',
                        'Aout' =>'Aout',
                        'Septembre' =>'Septembre',
                        'Octobre' =>'Octobre' ,
                        'Novembre' =>'Novembre',
                        'Décembre' =>'Décembre'
                    ])
                    ->label(__('Month')),
                Repeater::make('dishMenu')
                    ->relationship()
                    //->minItems(28)
                    ->maxItems(31)
                    //->defaultItems(30)
                    ->schema([
                        DatePicker::make('dish_at')
                            ->label(__('Date'))
                            ->native(false)
                            ->closeOnDateSelection(),
                        Select::make('soup_id')
                            ->label(__('soup'))
                            ->relationship('soupDish', 'name', fn ($query) => $query->where('type', 1)->orderby('name'))
                            ->searchable()
                            ->preload()
                            ->required(),
                        Select::make('dish_id')
                            ->label(__('Main course'))
                            ->relationship('mainCourseDish', 'name', fn ($query) => $query->where('type', 2)->orderby('name'))
                            ->searchable()
                            ->preload()
                            ->required(),
                        Select::make('dessert_id')
                            ->label(__('dessert'))
                            ->searchable()
                            ->relationship('dessertDish', 'name', fn ($query) => $query->where('type', 3)->orderby('name'))
                            ->preload()
                            ->required(),
                    ])
                    ->grid(5)
                    ->reorderable(false)
            ])
            ->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\Layout\Grid::make()
                ->columns(1)
                ->schema([
                    TextColumn::make('month')
                        ->label(__('date'))
                    ->searchable()
                ]),
            ])
            ->contentGrid([
                'md' => 3,
                'xl' => 7,
            ])
            ->paginated(false)
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMenus::route('/'),
            'create' => Pages\CreateMenu::route('/create'),
            'edit' => Pages\EditMenu::route('/{record}/edit'),
        ];
    }

    public static function getModelLabel(): string
    {
        return __('Menu');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Menus');
    }


}
