<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'client_id',
        'menu_id',
        'special_id',
        'date',
        'comment',
    ];

    public function client(): belongsTo
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
    public function dailyOrders(): HasMany
    {
        return $this->hasMany(DailyOrder::class, );
    }
}
