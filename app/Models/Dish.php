<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Dish extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'type',
        'is_active',
        'description'
    ];



    public function typeDish()
    {
        return $this->belongsTo(Type_Dish::class, 'type' , 'id');
    }
    public function intolerances(): BelongsToMany
    {
        return $this->belongsToMany(Intolerance::class, 'dish_intolerance');
    }

}
