<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class DailyOrder extends Model
{
    use HasFactory;

    protected $table = 'daily_order';

    protected $fillable = [
        'soup_number',
        'dish_number',
        'dessert_number',
        'dish_menu_id',
        'order_id',
        'order_at',
    ];

    public function order () :BelongsTo
    {
        return $this->belongsTo(Order::class,'order_id');
    }

    public function dishMenu () :BelongsTo
    {
        return $this->belongsTo(DishMenu::class,'dish_menu_id');
    }

    public function specials () : BelongsToMany
    {
        return $this->belongsToMany(Special::class, 'dish_order_special', 'daily_order_id','special_id');
    }

}
