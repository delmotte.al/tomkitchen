<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type_Dish extends Model
{
    use HasFactory;

    protected $table = 'type_dish';

    protected $fillable = [
        'name'
    ];
}
