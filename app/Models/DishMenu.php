<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class DishMenu extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dish_menu';

    protected $fillable = [
        'soup_id',
        'dish_id',
        'dessert_id',
        'menu_id',
        'dish_at',
    ];

    public function soupDish () :HasOne
    {
        return $this->hasOne(Dish::class,'id','soup_id');
    }
    public function mainCourseDish () :HasOne
    {
        return $this->hasOne(Dish::class,'id','dish_id');
    }
    public function dessertDish () :HasOne
    {
        return $this->hasOne(Dish::class,'id','dessert_id');
    }
    public function menu () :BelongsTo
    {
        return $this->belongsTo(Menu::class,'menu_id');
    }
}
