<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Menu extends Model
{
    use HasFactory;
    protected $fillable = [
        'month',
        'description',
    ];


    public function dishMenu(): HasMany
    {
        return $this->hasMany(DishMenu::class, 'menu_id');
    }
}
