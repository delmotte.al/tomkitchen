<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Special extends Model
{
    use HasFactory;
    protected $table = 'specials';
    protected $fillable = [
        'name',
        'status',
        'description'
    ];
    protected $casts = [
        'status' => 'boolean',
    ];

    public function intolerances(): BelongsToMany
    {
        return $this->belongsToMany(Intolerance::class, 'intolerance_special');
    }
}
