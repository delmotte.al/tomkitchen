<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'address',
        'phone',
        'email',
        'delivery_id',
    ];

    /**
     * @param Builder $query
     * @param string|null $search
     * @return void
     */
    public function scopeSearchBy(Builder $query, ?string $search): void
    {
        $query->where('name', 'LIKE', '%'.$search.'%')
            ->orWhere('address', 'LIKE', '%'.$search.'%')
            ->orWhere('email', 'LIKE', '%'.$search.'%')
            ->orWhere('delivery', 'LIKE', '%'.$search.'%');

    }

    public function deliveries(): BelongsTo
    {
        return $this->belongsTo(Delivery::class, 'delivery_id');
    }

    public function intolerances(): BelongsToMany
    {
        return $this->belongsToMany(Intolerance::class, 'client_intolerance');
    }
}
