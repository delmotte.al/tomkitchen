<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('statsData', () => ({
            stats: null,
            sortCol: null,
            sortAsc: false,
            curPage: 1,
            pageSize: {{ $pageSize }},
            async init() {
                let data = JSON.parse('@json($rows)')
                this.stats = data.map((d,i) => { d.id = i; return d; });
            },
            sort(col) {
                if(this.sortCol === col) this.sortAsc = !this.sortAsc;
                this.sortCol = col;
                this.stats.sort((a, b) => {
                    if(a[this.sortCol] < b[this.sortCol]) return this.sortAsc?1:-1;
                    if(a[this.sortCol] > b[this.sortCol]) return this.sortAsc?-1:1;
                    return 0;
                });
            },
            nextPage() {
                if((this.curPage * this.pageSize) < this.stats.length) this.curPage++;
            },
            previousPage() {
                if(this.curPage > 1) this.curPage--;
            },
            get pagedCats() {
                if(this.stats) {
                    return this.stats.filter((row, index) => {
                        let start = (this.curPage-1)*this.pageSize;
                        let end = this.curPage*this.pageSize;
                        if(index >= start && index < end) return true;
                    })
                } else return [];
            }
        }))
    });
</script>

<div x-data="statsData">

    <table class="table-auto w-full border text-sm">
        <thead>
            <tr>
                @foreach ($columns as $key => $column)
                    <th @click="sort('{{ $key }}')"
                        class="border-b border-e font-medium p-4 text-slate-400">
                        <div class="flex justify-between">
                            {{ $column }}
                            <template x-if="'{{$key}}' === sortCol && sortAsc">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5"/>
                                </svg>
                            </template>
                            <template  x-if="'{{$key}}' === sortCol && !sortAsc">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-short" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5A.5.5 0 0 1 8 4"/>
                                </svg>
                            </template>
                        </div>
                    </th>
                @endforeach
            </tr>
        </thead>
        <tbody class="bg-inherit">
        <template x-if="!stats">
            <tr>
                <td colspan="4">
                    <i>Loading...</i>
                </td>
            </tr>
        </template>
        <template :key="stat.id" x-for="stat in pagedCats">
            <tr>
                @foreach ($columns as $key => $column)
                    <td class="text-center border-b p-4" x-text="stat.{{ $key }}"></td>
                @endforeach
            </tr>
        </template>
        </tbody>
    </table>

    <div class="flex gap-2 justify-between pt-2">
        <div class="text-gray-500">
            <span x-text="curPage"></span> sur <span x-text="Math.ceil(stats.length / pageSize)"></span> page(s)
        </div>
        <div>
            <button class="border bg-primary px-2 py-1" @click="previousPage">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-short" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5"/>
                </svg>
            </button>
            <button class="border bg-primary px-2 py-1" @click="nextPage">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8"/>
                </svg>
            </button>
        </div>
    </div>

</div>
