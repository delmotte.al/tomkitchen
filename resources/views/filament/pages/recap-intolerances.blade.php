<x-filament-panels::page>

    <x-datatable
        :columns="[
            'intolerance' => 'Intolérances',
            'client' => 'Clients',
        ]"
        :rows="$this->get()->toArray()" />

</x-filament-panels::page>

