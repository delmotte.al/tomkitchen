<x-filament-panels::page>

    <x-datatable
        :columns="[
            'dish_menu_id' => 'Menu',
            'sum_soup' => 'Somme des soupes',
            'sum_dish' => 'Somme des repas',
            'sum_dessert' => 'Somme des desserts'
        ]"
        :rows="$this->get()->toArray()" />

</x-filament-panels::page>


