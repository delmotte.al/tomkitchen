<x-filament-panels::page>

    <x-datatable
        :columns="[
            'special' => 'Intolérances',
            'client' => 'Clients',
        ]"
        :rows="$this->get()->toArray()" />

</x-filament-panels::page>
